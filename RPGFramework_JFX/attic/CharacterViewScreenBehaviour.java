/**
 *
 */
package org.prelle.rpgframework.jfx;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.prefs.Preferences;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.jfx.CardTile.Side;

import javafx.collections.ListChangeListener;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.FlowPane;

/**
 * @author prelle
 *
 */
public class CharacterViewScreenBehaviour {

	private final static Logger logger = LogManager.getLogger(RPGFrameworkJFXConstants.BASE_LOGGER_NAME);

	private static Preferences CONFIG;

	private CharacterViewScreen control;
    private FlowPane flow;

	//-------------------------------------------------------------------
	/**
	 * @param control
	 * @param keyBindings
	 */
	public CharacterViewScreenBehaviour(CharacterViewScreen control, CharacterViewScreenSkin skin) {
		this.control = control;
       flow = skin.getFlow();

		if (control.getId()!=null) {
			logger.debug("CONFIG path "+"/org/rpgframework/"+control.getId());
			CONFIG = Preferences.userRoot().node("/org/rpgframework/"+control.getId());
		}

       restoreState();
       initInteractivity();
	}

	//-------------------------------------------------------------------
	private CharacterViewScreen getControl() { return control; }

	//-------------------------------------------------------------------
	/**
	 */
	private void initInteractivity() {
		for (CardTile card : getControl().getItems()) {
			setupCardDND(card);
		}

		((CharacterViewScreen)getControl()).getItems().addListener(new ListChangeListener<CardTile>() {

			@Override
			public void onChanged(javafx.collections.ListChangeListener.Change<? extends CardTile> c) {
				logger.debug("changed "+c);
				while (c.next()) {
					if (c.wasAdded()) {
						for (CardTile card : c.getAddedSubList())
							setupCardDND(card);
					} else
					if (c.wasRemoved()) {
						flow.getChildren().removeAll(c.getRemoved());
					} else
						System.err.println(getClass()+".initInteractivity: Operation "+c+" not supported");
				}
			}
		});

		/*
		 * CardTile positions can only be memorized if the CharacterViewScreen has an identifier
		 */
		getControl().idProperty().addListener( (ov,o,n) -> {
			if (n==null) {
				CONFIG = null;
				return;
			}
			CONFIG = Preferences.userRoot().node("/org/rpgframework/"+n);
			restoreState();
		});
	}

	//-------------------------------------------------------------------
	private void setupCardDND(CardTile card) {
		logger.debug("Setup "+card);
		card.setOnDragDetected(event -> dragStarted(event));
		card.setOnDragEntered (event -> dragEntered(event));
		card.setOnDragOver    (event -> dragOver(event));
		card.setOnDragExited  (event -> dragExited(event));
		card.setOnDragDropped (event -> dragDropped(event));
		card.visibleSideProperty().addListener( (ov,o,n) -> saveState());
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();
        Dragboard db = source.startDragAndDrop(TransferMode.ANY);

        ClipboardContent content = new ClipboardContent();
        content.putString(source.getId());
        logger.debug("Drag "+source.getId());
        db.setContent(content);

        WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
        db.setDragView(snapshot);

        event.consume();
    }

	//-------------------------------------------------------------------
	private void dragEntered(DragEvent event) {
		Node target = (Node) event.getSource();
		/* the drag-and-drop gesture entered the target */
	    /* show to the user that it is an actual gesture target */
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			target.getStyleClass().add("drop-target");
		}
         event.consume();
	}

	//-------------------------------------------------------------------
	private void dragExited(DragEvent event) {
		Node target = (Node) event.getSource();
		/* the drag-and-drop gesture entered the target */
	    /* show to the user that it is an actual gesture target */
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			target.getStyleClass().remove("drop-target");
		}
         event.consume();
	}

	//-------------------------------------------------------------------
	private void dragOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
            /* allow for both copying and moving, whatever user chooses */
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
	}

	//-------------------------------------------------------------------
	private void dragDropped(DragEvent event) {
		Node target = (Node) event.getSource();
		if (target.getId()==null) {
			logger.warn("Cannot drop before target without ID: "+target+"      source was "+event.getSource());
			return;
		}

	       /* if there is a string data on dragboard, read it and use it */
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String idToMove = db.getString();
            String idTarget = target.getId();
        	logger.debug("Move '"+idToMove+"' before '"+idTarget+"'");
        	// Find component for source ID
        	CardTile toMove = null;
        	for (CardTile node : getControl().getItems()) {
        		if (node.getId().equals(idToMove))
        			toMove = node;
        	}
        	if (toMove!=null) {
            	getControl().getItems().remove(toMove);
            	CardTile targetCard = null;
            	for (CardTile node : getControl().getItems()) {
            		if (node.getId().equals(idTarget))
            			targetCard = node;
            	}
               	int index = flow.getChildren().indexOf(targetCard);
            	logger.debug("Move "+toMove+" and insert at position of "+targetCard+" which is "+index+" from "+getControl().getItems());

            	if (index>0)
            		getControl().getItems().add(index, toMove);
            	else
            		getControl().getItems().add(toMove);
        	}


        	// Remove old card

        	// Re-insert
//        	int index = flow.getChildren().indexOf(target.getParent());
//        	flow.getChildren().add(index, toMove);
        	saveState();
           success = true;
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

	//-------------------------------------------------------------------
	private void saveState() {
		StringBuffer buf = new StringBuffer();
		for (Node node : flow.getChildren()) {
			String id = node.getId();
			if (node instanceof CardTile) {
				CardTile.Side side = ((CardTile)node).getVisibleSide();
				logger.trace("Visible side of "+node.getId()+" is "+side);
				switch (side) {
				case FRONT: id = "btn"+id; break;
				case BACK : id = "crd"+id; break;
				}
			}
			buf.append(id+" ");
		}
		if (CONFIG!=null)
			CONFIG.put("charview.order", buf.toString());
		logger.debug("saveState: "+buf);
	}

	//-------------------------------------------------------------------
	private void restoreState() {
		String value =null;
		if (CONFIG!=null)
			value = CONFIG.get("charview.order", null);
		logger.debug("restoreState: "+value);

		List<CardTile> toShow = new ArrayList<CardTile>(getControl().getItems());
		if (value!=null) {
			StringTokenizer tok = new StringTokenizer(value);
			while (tok.hasMoreTokens()) {
				String id = tok.nextToken();
				String baseId = id;
				if (id.startsWith("btn") || id.startsWith("crd"))
					baseId = id.substring(3);
				for (CardTile node : toShow) {
					if (node.getId()==null)
						continue;
					if (node.getId().equals(baseId)) {
						// Remove
						toShow.remove(node);
						// flip if required
						if (id.startsWith("crd")) {
							logger.debug("Flip "+baseId+" to BACK");
							node.setVisibleSide(Side.BACK);
						} else {
							logger.debug("Flip "+baseId+" to FRONT");
							node.setVisibleSide(Side.FRONT);
						}
						flow.getChildren().add(node);
						break;
					}
				}
			}
		}

		// Append all elements not previously added
		for (CardTile node : toShow) {
			logger.debug("* "+node);
			try {
				flow.getChildren().add(node);
			} catch (Exception e) {
				logger.error("Error adding CardTile "+node.getId());
				logger.error("Nodes where: "+value);
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(0);
			}
//			setupCardDND(node);
		}

	}

}
