/**
 * 
 */
package org.prelle.rpgframework.jfx;

import org.prelle.javafx.fluent.CommandBar;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

/**
 * @author prelle
 *
 */
public class SettingsAndCommandBar extends HBox {

	static final String DEFAULT_STYLE_CLASS = "settings-and-commandbar";
	
	private ObjectProperty<Node> settingsProperty;
	private ObjectProperty<CommandBar> commandBarProperty;

	//-------------------------------------------------------------------
	public SettingsAndCommandBar() {
		getStyleClass().setAll(DEFAULT_STYLE_CLASS);
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		settingsProperty = new SimpleObjectProperty<>();
		commandBarProperty = new SimpleObjectProperty<>();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Region buffer = new Region();
		buffer.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		HBox.setHgrow(buffer, Priority.ALWAYS);
		getChildren().add(buffer);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		settingsProperty.addListener( (ov,o,n) -> {
			if (o!=null)
				getChildren().remove(o);
			if (n!=null)
				getChildren().add(0, n);
		});
		commandBarProperty.addListener( (ov,o,n) -> {
			if (o!=null)
				getChildren().remove(o);
			if (n!=null) {
				getChildren().add(n);
				HBox.setHgrow(n, Priority.ALWAYS);
			}
		});
	}

	//-------------------------------------------------------------------
	public void setSettings(Node value) {
		settingsProperty.set(value);
	}

	//-------------------------------------------------------------------
	public void setCommandBar(CommandBar value) {
		commandBarProperty.set(value);
	}

}
