/**
 * 
 */
package org.prelle.rpgframework.jfx;

/**
 * @author spr
 *
 */
public class FrameworkJFXStarterLauncher {

	//-------------------------------------------------------------------
	/**
	 * To circumvent missing JavaFX-Runtime warning
	 * @param args
	 */
	public static void main(String[] args) {
		FrameworkJFXStarter.main(args);
	}

}
