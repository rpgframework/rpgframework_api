/**
 *
 */
package org.prelle.rpgframework.jfx;

import org.prelle.javafx.ModernUI;
import org.prelle.javafx.ResponsiveControlManager;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @author prelle
 *
 */
public class FrameworkJFXStarter extends Application {

	//-------------------------------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Application.launch(args);
	}

	//-------------------------------------------------------------------
	/**
	 */
	public FrameworkJFXStarter() {
	}

	@Override
	public void start(Stage stage) throws Exception {

		Region toShow = null;
		int variant = 1;
		switch (variant) {
		case 0:
			FreePointsNode fpNode = new FreePointsNode();
			fpNode.setStyle("-fx-border-color: black; -fx-border-width: 1px");
//			((FreePointsNode)toShow).setPoints(-2.0f);
			((FreePointsNode)fpNode).setName("EP");
			fpNode.setStyle("-fx-font-size: 400%");
			HBox foo = new HBox(fpNode, new Label("Some more text"));
			foo.setStyle("-fx-background-color: #c0c0c0");
			VBox foo2 = new VBox(foo, new Label("Next line"));
			toShow = foo2;
			break;
		case 1:
			CharacterDocumentView docView = new CharacterDocumentView();
			docView.setDescriptionHeading("Name of item");
			docView.setDescriptionPageRef("Pagereference");
			docView.setDescriptionText("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.\nLorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.");
			toShow = docView;
			break;
		}

//		HBox.setHgrow(toShow, Priority.SOMETIMES);
		ResponsiveControlManager.manageResponsiveControls(toShow, 640, 1008);
		Scene scene = new Scene(toShow, 500,500);
		ModernUI.initialize(scene);
		scene.getStylesheets().add(getClass().getResource("css/rpgframework.css").toExternalForm());
		stage.setScene(scene);
		stage.show();
	}
}
