/**
 * @author Stefan Prelle
 *
 */
module rpgframework.jfx {
	exports org.prelle.rpgframework.gamemaster.jfx;
	exports org.prelle.rpgframework.jfx;
	opens org.prelle.rpgframework.jfx.css;

	requires transitive javafx.extensions;
	requires java.prefs;
	requires javafx.base;
	requires transitive javafx.controls;
	requires transitive javafx.graphics;
	requires javafx.media;
	requires transitive rpgframework.api;
	requires transitive rpgframework.api.jfx;
	requires org.apache.logging.log4j;
	requires javafx.fxml;
}