/**
 * 
 */
package de.rpgframework;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.rpgframework.character.CharacterProvider;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.print.PrintManager;
import de.rpgframework.products.ProductService;

/**
 * @author prelle
 *
 */
public interface FunctionCharacterAndRules {
	
	//--------------------------------------------------------------------
	public List<RulePlugin<?>> getRulePlugins();
	
	//--------------------------------------------------------------------
	/**
	 * Return all plugins for a specific roleplaying system
	 */
	default public Collection<RulePlugin<?>> getRulePlugins(RoleplayingSystem rules) {
		ArrayList<RulePlugin<?>> ret = new ArrayList<RulePlugin<?>>();
		for (RulePlugin<?> tmp : getRulePlugins())
			if (tmp.getRules()==rules)
				ret.add(tmp);
		return ret;
	}

	public CharacterProvider getCharacterService();

	public ProductService getProductService();

	public PrintManager getPrintManager();

}
