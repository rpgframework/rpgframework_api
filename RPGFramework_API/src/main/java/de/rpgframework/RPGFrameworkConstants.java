package de.rpgframework;

import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public interface RPGFrameworkConstants {
	
	public final static ResourceBundle RES = ResourceBundle.getBundle("de.rpgframework.i18n.rpgframework");

}
