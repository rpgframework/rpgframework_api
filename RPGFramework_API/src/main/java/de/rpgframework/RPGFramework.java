/**
 *
 */
package de.rpgframework;

import de.rpgframework.boot.BootStep;
import de.rpgframework.boot.StandardBootSteps;
import de.rpgframework.character.CharacterProvider;
import de.rpgframework.core.LicenseManager;
import de.rpgframework.core.PlayerService;
import de.rpgframework.core.SessionService;
import de.rpgframework.devices.DeviceService;
import de.rpgframework.media.MediaService;
import de.rpgframework.media.Webserver;
import de.rpgframework.print.PrintManager;
import de.rpgframework.products.ProductService;
import de.rpgframework.session.SessionScreen;
import de.rpgframework.social.SocialNetworkProvider;

/**
 * @author prelle
 *
 */
public interface RPGFramework {

	public final static String PREFERENCE_PATH = "/de/rpgframework";
	public final static String PROP_LANGUAGE = "language";
	public final static String PROP_DATADIR  = "dataDir";
	public final static String PROP_PROMODIR  = "promoDir";
	public final static String PROP_UPDATE_ASK = "askUpdater";
	public final static String PROP_UPDATE_RUN = "runUpdater";
	public final static String PROP_RULE_LIMIT = "ruleLimit";

	public final static String LAST_OPEN_DIR = PREFERENCE_PATH+"/lastDir/open";
	public final static String LAST_SAVE_DIR = PREFERENCE_PATH+"/lastDir/save";
	public final static String PROP_LAST_OPEN_IMAGE_DIR = "image";
	public final static String PROP_LAST_SAVE_PRINT_DIR = "print";

	//-------------------------------------------------------------------
	public void addBootStep(StandardBootSteps roleplayingSystems);

	//-------------------------------------------------------------------
	public void addBootStep(BootStep step);

	public void initialize(RPGFrameworkInitCallback listener);


	public ConfigContainer getConfiguration();

	public ConfigContainer getPluginConfigurationNode();

	public FunctionCharacterAndRules getCharacterAndRules();

	public FunctionMediaLibraries getMediaLibraries();

	public FunctionSessionManagement getSessionManagement();

	public LicenseManager getLicenseManager();



	default CharacterProvider getCharacterService() {
		return getCharacterAndRules().getCharacterService();
	}

	default public ProductService getProductService() {
		return getCharacterAndRules().getProductService();
	}

	default public PrintManager getPrintManager() {
		return getCharacterAndRules().getPrintManager();
	}



	default public MediaService getMediaService() {
		return getMediaLibraries().getMediaService();
	}


	//	public void initialize(RoleplayingSystem... rules);

	default public PlayerService getPlayerService() {
		return getSessionManagement().getPlayerService();
	}

	default public SessionService getSessionService() {
		return getSessionManagement().getSessionService();
	}

	default public DeviceService getDeviceService() {
		return getSessionManagement().getDeviceService();
	}

	default public SocialNetworkProvider getSocialNetworkProvider() {
		return getSessionManagement().getSocialNetworkProvider();
	}

	default public Webserver getWebserver() {
		return getSessionManagement().getWebserver();
	}

	default public SessionScreen getSessionScreen() {
		return getSessionManagement().getSessionScreen();
	}


}
