/**
 *
 */
package de.rpgframework.boot;

/**
 * @author Stefan
 *
 */
public enum StandardBootSteps {

	FRAMEWORK_PLUGINS,
	ROLEPLAYING_SYSTEMS,
	PRODUCT_DATA,
	CUSTOM_DATA,
	CHARACTERS,
	GROUP_DATA,
	ENHANCED_ADVENTURE,
	MEDIA,
	;

}
