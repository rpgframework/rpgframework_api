/**
 * 
 */
package de.rpgframework;

import de.rpgframework.character.CharacterProviderFull;
import de.rpgframework.core.PlayerService;
import de.rpgframework.core.SessionService;
import de.rpgframework.devices.DeviceService;
import de.rpgframework.media.Webserver;
import de.rpgframework.session.SessionScreen;
import de.rpgframework.social.SocialNetworkProvider;

/**
 * @author prelle
 *
 */
public interface FunctionSessionManagement {

	public Webserver getWebserver();

	public SessionScreen getSessionScreen();

	public PlayerService getPlayerService();

	public CharacterProviderFull getCharacterService();

	public SessionService getSessionService();

	public DeviceService getDeviceService();

	public SocialNetworkProvider getSocialNetworkProvider();

}
