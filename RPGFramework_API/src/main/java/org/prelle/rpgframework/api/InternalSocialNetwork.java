/**
 * 
 */
package org.prelle.rpgframework.api;

import de.rpgframework.ConfigContainer;
import de.rpgframework.social.OnlineService;

/**
 * @author prelle
 *
 */
public interface InternalSocialNetwork extends OnlineService {

	//--------------------------------------------------------------------
	public void initialize(ConfigContainer parentConfig, InternalSocialNetworkCallback callback);

}
