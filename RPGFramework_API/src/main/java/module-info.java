module rpgframework.api {
	exports de.rpgframework;
	exports de.rpgframework.core;
	exports de.rpgframework.support.combat;
	exports de.rpgframework.addressbook;
	exports de.rpgframework.genericrpg;
	exports de.rpgframework.products;
	exports de.rpgframework.adventure;
	exports de.rpgframework.media.map;
	exports de.rpgframework.support.combat.map;
	exports de.rpgframework.session;
	exports de.rpgframework.social;
	exports de.rpgframework.print;
	exports de.rpgframework.music;
	exports de.rpgframework.genericrpg.chargen;
	exports de.rpgframework.genericrpg.modification;
	exports de.rpgframework.boot;
	exports de.rpgframework.worldinfo;
	exports de.rpgframework.media;
	exports de.rpgframework.sound;
	exports de.rpgframework.character;
	exports de.rpgframework.devices;
	exports org.prelle.online.contacts;
	exports org.prelle.rpgframework.api;

	uses de.rpgframework.RPGFramework;
	uses de.rpgframework.RulePlugin;
	uses de.rpgframework.RPGFrameworkPlugin;

	requires java.logging;
	requires java.prefs;
	requires java.sql;
	requires java.xml;
}