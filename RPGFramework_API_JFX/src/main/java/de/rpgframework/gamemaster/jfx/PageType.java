/**
 * 
 */
package de.rpgframework.gamemaster.jfx;

/**
 * @author prelle
 *
 */
public enum PageType {

	/**
	 * Gamemaster view
	 */
	CHARACTERS,
	/**
	 * Enhanced Adventure
	 */
	ADVENTURE,
	/**
	 * BattlefieldView and Initiative
	 */
	COMBAT,
	/**
	 * Audio, Video and Lights
	 */
	SCENE,
	
}
