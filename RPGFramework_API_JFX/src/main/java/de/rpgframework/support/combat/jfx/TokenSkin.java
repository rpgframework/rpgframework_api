package de.rpgframework.support.combat.jfx;

import de.rpgframework.support.combat.Combatant;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.SkinBase;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

class TokenSkin<C extends Combatant> extends SkinBase<Token<C>> {

	private Token<C> model;
	private Canvas canvas;
	private ImageView image;
	private Text text;
	private GraphicsContext gc;

	//-------------------------------------------------------------------
	protected TokenSkin(Token<C> model) {
		super(model);
		this.model = model;

		initComponents();
		initLayout();
		initInteractivity();

		imageRadiusChanged(32);

		model.imageRadiusProperty().addListener( (ov,o,n) -> {
			imageRadiusChanged((int)n);});
		model.imageRadiusProperty().addListener(new InvalidationListener() {
			public void invalidated(Observable observable) {
				System.out.println("TokenSkin: invalidated "+observable);
			}
		});

		refreshCanvas();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		image = new ImageView(model.getImage());
		image.setPreserveRatio(false);

		canvas = new Canvas(80, 80);

		text = new Text(model.getText());
		text.setStyle("-fx-font-size: 125%; -fx-fill: white; -fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.8), 2, 1, 1, 1);");

		gc = canvas.getGraphicsContext2D();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		model.impl_getChildren().addAll(canvas, image, text);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		model.rightValueMaxProperty().addListener( (ov,o,n) -> refreshCanvas());
		model.rightValueProperty().addListener( (ov,o,n) -> refreshCanvas());
		model.leftValueMaxProperty().addListener( (ov,o,n) -> refreshCanvas());
		model.leftValueProperty().addListener( (ov,o,n) -> refreshCanvas());
	}

	//-------------------------------------------------------------------
	private void refreshCanvas() {
		gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

		// Right value
		double perSegment = 180.0 / model.rightValueMaxProperty().get();
		double spacing = 1.5;
		double realPS  = perSegment - 2*spacing;

		gc.setFill(Color.GREEN);
		for (int i=0; i<model.getRightValueMax(); i++) {
			double from = 270 - spacing + i*perSegment;
			if (i<model.getRightValue())
				gc.fillArc(0, 0, 80, 80, from, realPS, ArcType.ROUND);
			else
				gc.strokeArc(0, 0, 80, 80, from, realPS, ArcType.ROUND);
		}

		// Left value
		perSegment = 180.0 / model.leftValueMaxProperty().get();
		spacing = 1.5;
		realPS  = perSegment - 2*spacing;

		gc.setFill(Color.RED);
		for (int i=0; i<model.leftValueProperty().get(); i++) {
			double from = 270 - spacing - i*perSegment;
			gc.fillArc(0, 0, 80, 80, from-realPS, realPS, ArcType.ROUND);
		}
	}

	//-------------------------------------------------------------------
	private void imageRadiusChanged(int newRadius) {
		image.setFitWidth(newRadius*2);
		image.setFitHeight(newRadius*2);
		image.setClip(new Circle(newRadius, newRadius, newRadius));
	}

}