module rpgframework.api.jfx {
	exports de.rpgframework.support.combat.jfx;
	exports de.rpgframework.adventure.jfx;
	exports de.rpgframework.gamemaster.jfx;

	uses de.rpgframework.gamemaster.jfx.SessionScreenPlugin;

	requires javafx.base;
	requires javafx.controls;
	requires transitive javafx.graphics;
	requires transitive rpgframework.api;
}